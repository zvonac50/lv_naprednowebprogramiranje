<!-- Student Comment
User interface that is used for showing main context after successful log-in.
Showing two tables, 
    first one is used for showing tables that are owned by logged user
    and second table is used for showing tables that owner is only a member of (not creator)
-->

@extends('projects.layout')
@section('content')
    <div class="container">
        <div class="row">
 
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Projects</div>
                    <div class="card-body">
                        <a href="{{ url('/project/create') }}" class="btn btn-success btn-sm" title="Add New Project">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Price</th>
                                        <th>Tasks</th>
                                        <th>Date(Started)</th>
                                        <th>Date(Ended)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($projects as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->naziv_projekta }}</td>
                                        <td>{{ $item->opis_projekta }}</td>
                                        <td>{{ $item->cijena_projekta }}</td>
                                        <td>{{ $item->obavljeni_poslovi }}</td>
                                        <td>{{ $item->datum_pocetka }}</td>
                                        <td>{{ $item->datum_zavrsetka }}</td>
                                        <td>
                                            <a href="{{ url('/project/' . $item->id . '/edit') }}" title="Edit Project"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit</button></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Price</th>
                                        <th>Tasks</th>
                                        <th>Date(Started)</th>
                                        <th>Date(Ended)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($projects_member as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->naziv_projekta }}</td>
                                        <td>{{ $item->opis_projekta }}</td>
                                        <td>{{ $item->cijena_projekta }}</td>
                                        <td>{{ $item->obavljeni_poslovi }}</td>
                                        <td>{{ $item->datum_pocetka }}</td>
                                        <td>{{ $item->datum_zavrsetka }}</td>
                                        <td>
                                            <a href="{{ url('/project/' . $item->id . '/edit') }}" title="Edit Project"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit</button></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection