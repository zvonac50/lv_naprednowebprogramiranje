<!-- Student Comment
User interface that is used for showing main context after successful log-in.
User interface that is used for creating new project.
For educational purposes, input types are not validated, while setting a string type in an integer input field throws an exception.
-->

@extends('projects.layout')
@section('content')
 
<div class="card">
  <div class="card-header">Project Add Page</div>
  <div class="card-body">
      <form action="{{ url('project') }}" method="post">
        {!! csrf_field() !!}
        <label>Name</label></br>
        <input type="text" name="naziv_projekta" id="naziv_projekta" class="form-control"></br>
        <label>Description</label></br>
        <input type="text" name="opis_projekta" id="opis_projekta" class="form-control"></br>
        <label>Price</label></br>
        <input type="text" name="cijena_projekta" id="cijena_projekta" class="form-control"></br>
        <label>Tasks</label></br>
        <input type="text" name="obavljeni_poslovi" id="obavljeni_poslovi" class="form-control"></br>
        <label>Starting Date</label>
        <input type="date" name="datum_pocetka" id="datum_pocetka" class="form-control"></br>
        <label>Ending Date</label>
        <input type="date" name="datum_zavrsetka" id="datum_zavrsetka" class="form-control"></br>
        <div id="selected_members">Select team members: </div>
            <fieldset id="checkboxes">
                @foreach ($users as $user)
                    <div>
                        {{ $user->name }}<input type="checkbox" name="team_members[]" value={{ $user->id }}
                            class='member_checkbox' style="margin-left:5px">
                    </div>
                @endforeach
            </fieldset>

        <input type="submit" value="Save" class="btn btn-success", style="margin-top:25px"></br>
    </form>
   
  </div>
</div>
 
@stop