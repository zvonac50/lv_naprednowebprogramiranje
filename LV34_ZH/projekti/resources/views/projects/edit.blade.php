<!-- Student Comment
User interface that is used for showing main context after successful log-in.
User interface that is used for editting context of specific project.
If logged user is:
    Creator of project = can modify any context info
    Member of project = can modify only task context info
-->

@extends('projects.layout')
@section('content')
 
<div class="card">
  <div class="card-header">Project Edit Page</div>
  <div class="card-body">
      <form action="{{ url('project/' .$projects->id) }}" method="post">
        {!! csrf_field() !!}
        @method("PATCH")

        @if(auth()->user()->id == $projects->voditelj_id)
        <label>Name</label></br>
        <input type="text" name="naziv_projekta" id="naziv_projekta" value="{{$projects->naziv_projekta}}" class="form-control"></br>
        <label>Description</label></br>
        <input type="text" name="opis_projekta" id="opis_projekta" value="{{$projects->opis_projekta}}" class="form-control"></br>
        <label>Price</label></br>
        <input type="text" name="cijena_projekta" id="cijena_projekta" value="{{$projects->cijena_projekta}}" class="form-control"></br>
        <label>Starting Date</label>
        <input type="date" name="datum_pocetka" id="datum_pocetka" value="{{$projects->datum_pocetka}}" class="form-control"></br>
        <label>Ending Date</label>
        <input type="date" name="datum_zavrsetka" id="datum_zavrsetka" value="{{$projects->datum_zavrsetka}}" class="form-control"></br>
        @endif
        <label>Tasks</label></br>
        <input type="text" name="obavljeni_poslovi" id="obavljeni_poslovi" value="{{$projects->obavljeni_poslovi}}" class="form-control"></br>
        
        <input type="submit" value="Update" class="btn btn-success"></br>
    </form>
  </div>
</div>
 
@stop