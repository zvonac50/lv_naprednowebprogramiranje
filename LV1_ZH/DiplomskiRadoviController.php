<?php

// Include - omogućava korištenje 3rd party biblioteke i napisane klase za pojedine diplomske radove
include('./simple_html_dom.php');
include('./DiplomskiRadovi.php');

// Klasa za olakšano korištenje biblioteke za parsiranje HTML-DOM skripte sa dohvaćenog URL-a
class DiplomskiRadoviController
{
    private $html_parser;
    private $base_url;

    // Inicijalizacija 3rd party transkriptora i zadanog URL-a
    public function __construct($url)
    {
        $this->html_parser = new simple_html_dom();
        $this->base_url = $url;
    }

    // Metoda za dohvaćanje skripte sa zadanog URL-a
    public function fetch_data()
    {
        // Korištenje 'CURL-a' za dohvaćanje informacija za zadanog URL-a, postavljanje dodatnih opcija (npr. Timeout)
        $curl = curl_init($this->base_url);
        curl_setopt($curl, CURLOPT_FAILONERROR, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 3);
        $results = curl_exec($curl);

        curl_close($curl);
        return $this->parse_data($results);
    }

    // Skripta za parsiranje HTML elemenata, parsiranje se vrši pomoću example skripte 3rd party biblioteke
    private function parse_data($data)
    {
        $oibs = [];
        $hrefs = [];
        $titles = [];
        $texts = [];

        // Inicijaliza i parsiranje objekta sa podacima iz dane DOM skripte - metode iz 3rd party biblioteke
        $html = $this->html_parser->load($data);
        foreach ($html->find('img') as $img) {
            if (strpos($img, "logos") !== false) {
                array_push($oibs, preg_replace('/[^0-9]/', '', $img->src));
            }
        }

        foreach ($html->find('article') as $article) {
            foreach ($article->find('ul.slides img') as $img) {
            }
            foreach ($article->find('h2.entry-title a') as $link) {
                array_push($hrefs, $link->href);
                array_push($titles, $link->plaintext);
            }
        }

        // Dohvaćanje skripte sa svakog pojedinog URL-a
        $texts = $this->get_texts($hrefs);
        return array($oibs, $hrefs, $titles, $texts);
    }

    // Metoda za dohvaćanje konteksta HTML-a sa pojedinih URL-ova
    private function get_texts($hrefs)
    {
        $texts = [];

        foreach ($hrefs as $href) {
            $curl = curl_init($href);
            curl_setopt($curl, CURLOPT_FAILONERROR, 1);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_TIMEOUT, 5);
            $result = curl_exec($curl);
            curl_close($curl);

            $html = $this->html_parser->load($result);

            foreach ($html->find('.post-content') as $text) {
                array_push($texts, $text->plaintext);
            }
        }

        return $texts;
    }
}
