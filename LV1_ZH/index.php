<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laboratorijske vježbe - vježba 1</title>
</head>

<body>
    <?php
    include("./DiplomskiRadoviController.php");
    $dipl_radovi_controller = new DiplomskiRadoviController('http://stup.ferit.hr/index.php/zavrsni-radovi/page/5');
    $dipl_radovi = $dipl_radovi_controller->fetch_data(); // Inicijalizacija kontrolera i dohvaćanje podataka sa zadanoga URL-a

    // Inicijalizacija klase i postavljanje vrijednosti atributa
    $dipl_rad = new DiplomskiRadovi(
        array(
            'naziv_rada' => "",
            'tekst_rada' => "",
            'link_rada' => "",
            'oib_tvrtke' => ""
        )
    );

    // Upotreba inicijalizirane klase za spremanje i svakoga dohvaćenog diplomskoga rada sa zadanog URL-a preko kontroler klase
    for ($i = 0; $i < count($dipl_radovi[0]); $i++) {
        $dipl_rad->create(array(
            'naziv_rada' => $dipl_radovi[2][$i],
            'tekst_rada' => $dipl_radovi[3][$i],
            'link_rada' => $dipl_radovi[1][$i],
            'oib_tvrtke' => $dipl_radovi[0][$i]
        ));

        $dipl_rad->save(); // Spremanje pojedinoga diplomskoga rada u bazu podataka
    }

    $dipl_rad->read(); // Čitanje i prikaz svih diplomskih radova spremljenih u bazi podataka
    ?>
</body>

</html>