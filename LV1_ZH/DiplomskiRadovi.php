<?php

use iRadovi as GlobalIRadovi;

// Interface - obavezna implementacija navedenih metoda
interface iRadovi
{
    public function create($data);
    public function save();
    public function read();
}

// Klasa - implementira interface metode i služi kao kontejner za infromacije od pojedinom diplomskom radu
class DiplomskiRadovi implements GlobalIRadovi
{
    private $naziv_rada;
    private $tekst_rada;
    private $link_rada;
    private $oib_tvrtke;

    // Konstruktor klase - omogućava stvaranje objekta sa inicijaliziranim podacima
    function __construct($data)
    {
        $this->naziv_rada = $data['naziv_rada'];
        $this->tekst_rada = $data['tekst_rada'];
        $this->link_rada = $data['link_rada'];
        $this->oib_tvrtke = $data['oib_tvrtke'];
    }

    // Metoda za poziv konstruktora klase
    function create($data)
    {
        self::__construct($data);
    }

    // Spremanje pojedinog diplomskog rada u bazu podataka
    function save()
    {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "radovi";

        // Spajanje na lokalnu bazu sa zadanim lokalnim vjerodajnicama
        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $id = uniqid();
        $naziv = $this->naziv_rada;
        $tekst = $this->tekst_rada;
        $link = $this->link_rada;
        $oib = $this->oib_tvrtke;

        // Umetanje podataka za pojedini diplomski rad u bazu podataka
        $sql = "INSERT INTO `diplomski_radovi` (`id`, `naziv_rada`, `tekst_rada`, `link_rada`, `oib_tvrtke`) VALUES ('$id', '$naziv', '$tekst', '$link', '$oib')";

        if (!$conn->query($sql)) {
            echo "Error! " . $sql . "<br>" . $conn->error;
        };

        $conn->close();
    }

    // Metoda za dohvaćanje 
    function read()
    {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "radovi";

        // Spajanje na lokalnu bazu sa zadanim lokalnim vjerodajnicam
        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        // Čitanje i prikazivanje svih diplomskih radova unutar tablice "diplomski_radovi" u bazi "radovi" i ispis istih
        $sql = "SELECT * FROM `diplomski_radovi`";
        $output = $conn->query($sql);
        if ($output->num_rows > 0) {
            while ($item = $output->fetch_assoc()) {
                echo "<br><br><br>ID: " . $item["id"] .
                    "<br><br>OIB tvrtke: " . $item["oib_tvrtke"] .
                    "<br><br>Naziv rada: " . $item["naziv_rada"] .
                    "<br><br>Link rada: " . $item["link_rada"] .
                    "<br><br>Tekst rada: " . $item["tekst_rada"];
            }
        }

        $conn->close();
    }
}
