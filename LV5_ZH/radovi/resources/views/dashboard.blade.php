<!-- Student Comment
User interface that is used for communicating with user about successful or unsuccessful logging into upcomming context.
-->

@extends('layout')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>
  
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
  
                    You are Logged In, but something went wrong, please contact developers.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection