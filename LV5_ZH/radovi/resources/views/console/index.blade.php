<!-- Student Comment
User interface that is used for showing main context after successful log-in.
Showing table is used for showing users that can be manipulated with by changing role settings (admin only)
-->

@extends('console.layout')
@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Admin Console</div>
                <div class="card-body">
                    <br />
                    <br />
                    <div class="table-responsive">
                        <table class="table">
                            @csrf
                            @if (auth()->user()->role == "admin")
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->role }}</td>
                                    <td>
                                        <form action="/user" method="POST" id="create_project_form">
                                            @csrf
                                            @method('PUT')
                                            <div class="ml-5">
                                                @if($item->role == "student")
                                                <input type="submit" value="Promote" class="btn btn-success">
                                                @else
                                                <input type="submit" value="Demote" class="btn btn-danger">
                                                @endif
                                            </div>
                                            <input type="text" hidden name="id" value="{{ $item->id }}">
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection