<!-- Student Comment
User interface that is used for showing main context after successful log-in.
Showing table is used to let student to pick which project he wants to claim and get mentored for.
-->

@extends('student.layout')
@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Student</div>
                <div class="card-body">
                    <br />
                    <br />
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>naziv_rada</th>
                                    <th>naziv_rada_en</th>
                                    <th>zadatak_rada</th>
                                    <th>tip_studija</th>
                                    <th>nastavnik</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($projects as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->naziv_rada }}</td>
                                    <td>{{ $item->naziv_rada_en }}</td>
                                    <td>{{ $item->zadatak_rada }}</td>
                                    <td>{{ $item->tip_studija }}</td>
                                    <td>{{ $item->nastavnik }}</td>
                                    <td>
                                        <form action="/prijava" method="POST" id="create_project_form">
                                            @csrf
                                            @method('PUT')
                                            <div class="ml-5">
                                                <input type="submit" value="Upiši" class="btn btn-success">
                                            </div>
                                            <input type="text" hidden name="id" value="{{ $item->id }}">
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection