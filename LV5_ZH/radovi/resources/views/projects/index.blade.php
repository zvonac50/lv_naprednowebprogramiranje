<!-- Student Comment
User interface that is used for showing main context after successful log-in.
Showing table is used for role "nastavnik" to choose which student will get mentored by him.
-->

@extends('projects.layout')
@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Projects [Prijave]</div>
                <div class="card-body">
                    <a href="{{ url('/projects/create') }}" class="btn btn-success btn-sm" title="Add New Project">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New Project </a>
                    <br />
                    <br />
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Naziv rada</th>
                                    <th>Naziv rada [En]</th>
                                    <th>Zadatak rada</th>
                                    <th>Tip studija</th>
                                    <th>Prijave</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($projects as $index => $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->naziv_rada }}</td>
                                    <td>{{ $item->naziv_rada_en }}</td>
                                    <td>{{ $item->zadatak_rada }}</td>
                                    <td>{{ $item->tip_studija }}</td>
                                    @foreach($applicants[$index] as $mItem)
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td col="2">{{ $mItem->name }}
                                        <form action="/select" method="POST" id="create_project_form">
                                            @csrf
                                            @method('PUT')
                                            <input type="submit" value="Select" class="btn btn-success">
                                            <input type="text" hidden name="id" value="{{ $mItem->id }}">
                                            <input type="text" hidden name="project_id" value="{{ $item->id }}">
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection