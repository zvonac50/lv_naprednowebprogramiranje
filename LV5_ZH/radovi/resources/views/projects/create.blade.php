<!-- Student Comment
User interface that is used for showing main context after successful log-in.
User interface that is used for creating new project.
    [!] Locale settings (Task 4) has been applied here and can be tried with site lowest 2 buttons.
-->

@extends('projects.layout')
@section('content')

<div class="card">
    <div class="card-header">{{ __('messages.taskName') }}</div>
    <div class="card-body">
        <form action="/task" method="POST" id="create_tasks">
            @csrf
            @if (auth()->user()->role == "nastavnik")
            <label for="naziv_rada">{{ __('messages.taskName') }}</label><br>
            <input type="text" name="naziv_rada" id="naziv_rada" required class="formInputs"><br>

            <label for="naziv_rada_en">{{ __('messages.taskNameEn') }}</label><br>
            <input type="text" name="naziv_rada_en" id="naziv_rada_en" required class="formInputs"><br>

            <label for="zadatak_rada">{{ __('messages.description') }}</label><br>
            <input name="zadatak_rada" id="zadatak_rada" required class="formInputs"></input><br><br>
            <div id="study_type">
                <input type="radio" id="type1" name="type" value="stručni">
                <label for="type1">{{ __('messages.profStudProg') }}</label><br>
                <input type="radio" id="type2" name="type" value="preddiplomski">
                <label for="type2">{{ __('messages.undergraduate') }}</label><br>
                <input type="radio" id="type3" name="type" value="diplomski" checked="checked">
                <label for="type3">{{ __('messages.graduate') }}</label><br><br>
            </div>

            <button type="submit">{{ __('messages.createTask') }}</button>
            @endif
        </form>
    </div>

</div>

<br><br>
<a class="btn btn-success" href="{{ route('lang.switch', 'hr') }}">Hrvatski</a>
<a class="btn btn-success" href="{{ route('lang.switch', 'en') }}">English</a>

@stop