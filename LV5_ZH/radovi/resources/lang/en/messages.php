<?php

/* Student Comment
Method Descriptions.
    File usage: Creating custom string that can be dinamically used, depending on locale language option.
*/

return [
    //messages used in the application
    'createTask' => 'Create Task',
    'changeLangToCro' => 'Change language to croatian',
    'changeLangToEng' => 'Change language to english',
    'taskName' => 'Task name:',
    'taskNameEn' => 'Task name on english:',
    'description' => 'Description:',
    'profStudProg' => 'Professional study programme',
    'undergraduate' => 'Undergraduate',
    'graduate' => 'Graduate',
];
