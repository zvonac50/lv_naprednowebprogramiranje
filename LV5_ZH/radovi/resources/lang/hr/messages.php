<?php

/* Student Comment
Method Descriptions.
    File usage: Creating custom string that can be dinamically used, depending on locale language option.
*/

return [
    'createTask' => 'Dodajte novi rad',
    'changeLangToCro' => 'Promjeni jezik na hrvatski',
    'changeLangToEng' => 'Promjeni jezik na engleski',
    'taskName' => 'Naziv rada:',
    'taskNameEn' => 'Naziv rada na engleskom:',
    'description' => 'Zadatak rada:',
    'profStudProg' => 'Stručni studij',
    'undergraduate' => 'Preddiplomski',
    'graduate' => 'Diplomski',
];
