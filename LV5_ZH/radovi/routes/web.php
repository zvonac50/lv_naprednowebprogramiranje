<?php

/* Student Comment
Method Descriptions.
    File used for routing resources and methods for specific view usage or usage of specific method with approriate phrase.
*/

use App\Http\Controllers\AdminController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout');
});

Route::get('login', [AuthController::class, 'index'])->name('login');
Route::post('post-login', [AuthController::class, 'postLogin'])->name('login.post');
Route::get('registration', [AuthController::class, 'registration'])->name('register');
Route::post('post-registration', [AuthController::class, 'postRegistration'])->name('register.post');
Route::get('dashboard', [AuthController::class, 'dashboard']);
Route::get('logout', [AuthController::class, 'logout'])->name('logout');

Route::resource('/console', AdminController::class);
Route::put('/user', 'App\Http\Controllers\AdminController@update');

Route::resource('/projects', TaskController::class);
Route::post('/task', 'App\Http\Controllers\TaskController@save');
Route::put('/select', 'App\Http\Controllers\TaskController@select');

Route::resource('/tasks', StudentController::class);
Route::put('/prijava', 'App\Http\Controllers\StudentController@save');

Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'App\Http\Controllers\LanguageController@switchLang']);
