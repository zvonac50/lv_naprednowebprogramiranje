<?php

/* Student Comment
Method Descriptions.
    Model usage: Store and fetch exact data for specific table.
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;
    //cast array <=> json   
    protected $casts = [
        'studenti' => 'array'
    ];
}
