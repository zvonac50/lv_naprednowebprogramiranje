<?php

/* Student Comment
Method Descriptions.
    Model usage: Store and fetch exact data for specific table.
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'tasks';
    protected $primaryKey = 'id';
    protected $fillable = ['naziv_rada', 'naziv_rada_en', 'zadatak_rada', 'tip_studija', 'nastavnik', 'studenti', 'izabrani_student', 'created_at', 'updated_at'];

    //cast array <=> json   
    protected $casts = [
        'studenti' => 'array'
    ];
}
