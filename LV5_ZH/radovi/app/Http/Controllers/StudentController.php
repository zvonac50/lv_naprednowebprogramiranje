<?php

/* Student Comment
Method Descriptions.
    [1]index() -> console.index startup method which fetches all registered users and feed a view with it.
        [1.1] fetches only projects that aren't chosen already or which current user hasn't selected yet.
    [2]save() -> Method for saving current user.id to list of students for specific task.
*/

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\Project;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Task::Where('izabrani_student', NULL)->whereJsonDoesntContain('studenti', auth()->user()->id)->get();

        return view('student.index')->with('projects', $projects);
    }

    function save(Request $request)
    {
        $project = Task::find($request->id);
        $studenti = $project->studenti;
        array_push($studenti, auth()->user()->id);
        $project->studenti = $studenti;
        $project->update();

        return redirect('/tasks');
    }
}
