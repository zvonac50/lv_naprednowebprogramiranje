<?php

/* Student Comment
Method Descriptions.
    [1]index() -> method that fetches all available projects and feeds view with given info.
        [1.2] fetching all projects that hasn't been claimed, that are owned by current user and with some users registered as willing to claim
        [1.2] 2D array containing projects and users for each project is being passed to view as data feed.
    [2]save() -> method that creates new task with user given info and saved to database.
    [3]select() -> method that binds specifis user to specific project. "Teacher confirming a student as his mentor"
*/

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\User;
use Illuminate\Support\Facades\App;

class TaskController extends Controller
{
    function create()
    {
        return view('projects.create');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Task::Where('izabrani_student', NULL)->Where('nastavnik', auth()->user()->id)->whereJsonLength('studenti', '!=', 0)->get();

        $applicants = [];
        $counter = 0;
        foreach ($projects as $project) {
            $studenti = $project->studenti;
            $student_counter = 0;

            foreach ($studenti as $student) {
                $applicants[$counter][$student_counter] = User::find($student);
                $student_counter++;
            }

            $counter++;
        }

        return view('projects.index')->with('projects', $projects)->with('applicants', $applicants);
    }

    function save(Request $request)
    {
        $task = new Task();
        $task->naziv_rada = $request->naziv_rada;
        $task->naziv_rada_en = $request->naziv_rada_en;
        $task->zadatak_rada = $request->zadatak_rada;
        $task->tip_studija = $request->type;
        $task->nastavnik = auth()->user()->id;
        $task->save();

        return redirect('/projects');
    }

    function select(Request $request)
    {
        $task = Project::find($request->project_id);
        $task->izabrani_student = $request->id;
        $task->save();

        return redirect('/projects');
    }
}
