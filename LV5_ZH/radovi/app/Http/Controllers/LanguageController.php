<?php

/* Student Comment
Class used for changing Session Locale language, tightly coupled with middleware class, language config and kernel config files.
Currently supported 2 languages: hr and en
*/

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config as FacadesConfig;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{
    public function switchLang($lang)
    {
        if (array_key_exists($lang, FacadesConfig::get('languages'))) {
            Session::put('applocale', $lang);
        }
        return Redirect::back();
    }
}
