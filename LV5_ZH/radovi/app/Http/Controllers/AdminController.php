<?php

/* Student Comment
Method Descriptions.
    [1]index() -> console.index startup method which fetches all registered users and feed a view with it.
    [2]update() -> method for changing role nastavnik <-> student for specific registered user.
*/

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userID = auth()->user()->id;
        $users = User::where('id', '!=', $userID)->get();

        return view('console.index')->with('users', $users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = User::find($request->id);
        $user->role = $user->role == "student" ? "nastavnik" : "student";
        $user->save();

       return redirect('/console');
    }
}
