<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="index.css">
    <title>Zadatak 3</title>
</head>

<body>
    <?php
    // Interprets an XML file into an object
    $xml = simplexml_load_file("LV2.xml");

    // Creating start HTML Body tags
    $content = "<main>\n<ul> ";

    // For each element with "record" tag in parsed XML object, fetch and show all inner elements
    foreach ($xml->record as $profile) {
        // Fetch all inner record data
        $id = $profile->id;
        $first_name = $profile->ime;
        $last_name = $profile->prezime;
        $email = $profile->email;
        $sex = $profile->spol;
        $image = $profile->slika;
        $biography = $profile->zivotopis;
        // Creating each profile individually by pulling inner data from each record
        $content = $content .
            "<li>
                <div class='profile'>
                    <img src=$image alt='Profile picture' id='profilePicture'>
                    <h4>$first_name $last_name</h4>
                    <div class='personal_data'>
                        <p><b>Sex</b>: $sex</p>
                        <p><b>Email</b>: $email</p>
                        <p><b>Biography</b>: $biography</p>
                    </div>
                </div>
            </li>";
    }

    // Create ending HTML Body tags
    $content = $content . "</ul>\n</main>";
    // Show formed HTML Body tags with listed profiles
    echo $content;
    ?>
</body>

</html>