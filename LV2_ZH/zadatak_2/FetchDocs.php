<?php
$dir_path = "uploads/";

session_start();

// Checking if there is directory with files from previous steps [Upload]
if (!is_dir($dir_path))
    die("<p>Directory {$dir_path} doesn't exist.</p>");

// Fetching all files from given directory
$files = array_diff(scandir($dir_path), array('..', '.'));
if (count($files) === 0)
    die("<p>There aren't any files for decryption in folder \"{$dir_path}\"</p>");

// Creating list and adding element for each found file that first has to be decrypted with another script
echo "<ul>";
foreach ($files as $file) {
    $file_name = substr($file, 0, strlen($file) - 4);
    echo "<li><a href=\"Download.php?file={$file_name}\">{$file_name}</a></li>";
}
echo "</ul>";
