<?php
$dir_path = "uploads/";

// Encryption global data, choose encryption algorithm and secret key
$encryption_string = 'Secret Key';
$encryption_cypher = "aes-128-cbc-hmac-sha1";
$encryption_options = 0;

session_start();

// Get encrypted file path and prepare decryption variables
$file_name = $_GET['file'];
$decryption_key = md5($encryption_string);
$decryption_iv = $_SESSION['iv'];
$file_content_encrypted = file_get_contents("{$dir_path}{$file_name}.txt");

// Decrypt content of pulled file with OpenSSL library
$file_content_decrypted = base64_decode($file_content_encrypted);
$content_encrypted = openssl_decrypt($file_content_decrypted, $encryption_cypher, $decryption_key, $encryption_options, $decryption_iv);

// Save decrypted content of pulled file in given file name
$file_decrypted = "{$dir_path}{$file_name}";
file_put_contents($file_decrypted, $content_encrypted);

// If encryption has succeeded, pull file from server and unlink it.
if (file_exists($file_decrypted)) {
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . basename($file_decrypted) . '"');
    ob_clean();
    readfile($file_decrypted);
    unlink($file_decrypted);

    die();
}
