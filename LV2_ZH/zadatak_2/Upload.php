<?php
// Encryption global data, choose encryption algorithm and secret key
$encryption_string = 'Secret Key';
$encryption_cypher = "aes-128-cbc-hmac-sha1";
$encryption_options = 0;

session_start();

// Getting informations about file uploaded from personal computer [step 1]
$file_name = $_FILES['file']['name'];
$file_path = "uploads/{$file_name}";

// Filtering files uploaded in previous step [step 2]
$file_type = pathinfo($file_path, PATHINFO_EXTENSION);
$file_extensions = array("pdf", "jpeg", "png");
if (!in_array(strtolower($file_type), $file_extensions))
    die("<p>Invalid file extension!</p>");

// Using OpenSSL library for encrypting content in file uploaded in previous 2 steps 
$file_content = file_get_contents($_FILES['file']['tmp_name']);
$encryption_key = md5($encryption_string);
$iv_length = openssl_cipher_iv_length($encryption_cypher);
$encryption_iv = random_bytes($iv_length);
$file_encrypted = openssl_encrypt($file_content, $encryption_cypher, $encryption_key, $encryption_options, $encryption_iv);

// Encoding data with MIME base64 and saving it as a string
$encrypted_data = base64_encode($file_encrypted);
$_SESSION['iv'] = $encryption_iv;
$file_name_no_extension = substr($file_name, 0, strpos($file_name, "."));
if (!is_dir("uploads/"))
    if (!mkdir("uploads/", 0777, true))
        die("<p>Couldn't create \"uploads/\" directory.</p>");

// Saving encoded data in textual file that is ready to be uploaded on server
$file_encrypted_path = "uploads/{$file_name_no_extension}.{$file_type}.txt";
file_put_contents($file_encrypted_path, $encrypted_data);

echo "<p>Encrypted file uploaded successfully.";
