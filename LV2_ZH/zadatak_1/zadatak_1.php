<?php
// Database credentials and global variables
$db_name = 'radovi';
$dir_path = "backup/$db_name";
$backup_seq = 0;

// Creating/Opening directory
if (!is_dir($dir_path)) {
    if (mkdir($dir_path, 0777, true))
        echo "<p>Directory has been created successfully.</p></body></html>";
    else
        die("<p>Can not create directory.</p></body></html>");
}

// Creating connection with local database
$conn = new mysqli("localhost", "root", "", $db_name);
if ($conn->connect_error)
    die("<p>Couldn't connect to $db_name.</p>");
// Pulling all available tables located in connected database
$tables = $conn->query('SHOW TABLES');
if ($tables->num_rows > 0) {
    // For each pulled table, list through all rows and values(columns)
    while (list($table) = $tables->fetch_array(MYSQLI_NUM)) {
        $sql = "SELECT * FROM $table";
        $table_attributes = $conn->query($sql);
        $columns = $table_attributes->fetch_fields();

        if ($table_attributes->num_rows > 0) {
            $backup_seq++;
            $file_name = "$dir_path/{$table}_{$backup_seq}";

            // Open file for a table and write all iterated elements in specific way [1. row\r\n2. row]
            if ($fd = fopen("{$file_name}.txt", 'w')) {
                while ($row = $table_attributes->fetch_array(MYSQLI_NUM)) {
                    // Add first row as specified in task and list columns of current table as parameters
                    fwrite($fd, "INSERT INTO $db_name (");
                    foreach ($columns as $column) {
                        fwrite($fd, "$column->name");
                        if ($column != end($columns))
                            fwrite($fd, ", ");
                    }
                    fwrite($fd, ")\r\n");

                    // Add Second row as specified in task and list values of current table as parameters
                    fwrite($fd, "VALUES (");
                    foreach ($row as $row_value) {
                        $row_value = addslashes($row_value);
                        fwrite($fd, "'$row_value'");
                        if ($row_value != end($row))
                            fwrite($fd, ", ");
                        else
                            fwrite($fd, ")\";");
                    }
                    fwrite($fd, "\r\n");
                }
                fclose($fd);
                echo "<p>Backup table[$table] has been created and filled.</p>";

                // Open file for a compressed table with same name but with a different extension, pull content from previous file and write it into a current compressed file
                if ($fd = gzopen("{$file_name}.sql.gz", 'w9')) {
                    gzwrite($fd, file_get_contents("{$file_name}.txt"));
                    gzclose($fd);
                } else
                    echo "<p>There was a problem while compressing file[{$file_name}]</p>";
            } else
                echo "<p>There was a problem while opening file[{$file_name}]</p>";
        }
    }
} else
    echo "<p>There is no available tables in $db_name</p>";
