var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

/*
Any requests to this controller must pass through this 'use' function
Copy and pasted from method-override
*/
router.use(bodyParser.urlencoded({ extended: true }))
router.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        var method = req.body._method
        delete req.body._method
        return method
    }
}))

/*
Simple interface that asks DB if given info exists in datasets.
If exists, login user and let him on main page.
*/
router.route('/')
    .get(function (req, res, next) {
        const data = {
            "email": "",
            "password": ""
        }

        res.render('auth/login', {
            "data": data,
            "title": "Login",
        });
    })
    .post(function (req, res) {
        const email = req.body.email;
        const password = req.body.password;

        const data = {
            "email": email,
            "password": password
        }

        mongoose.model('User').findOne({ email: email, password: password }, function (err, user) {
            if (err) {
                return console.error(err);
            } else {
                if (user) {
                    req.session.uid = user.id;
                    res.redirect('/');
                } else {
                    const error = "Invalid credentials. Try again."
                    res.format({
                        html: function () {
                            res.render('auth/login', {
                                "error": error,
                                "data": data,
                                "title": "Login",
                            });
                        }
                    });
                }
            }
        }
        )
    });

module.exports = router;
