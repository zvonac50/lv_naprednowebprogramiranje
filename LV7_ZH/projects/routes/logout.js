var express = require('express');
var router = express.Router();

/*
Loging current user out.
Destroying current session.
*/
router.route('/')
    .get(function (req, res, next) {
        req.session.destroy();
        res.redirect('/login');
    });

module.exports = router;
