var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

/*
Any requests to this controller must pass through this 'use' function.
Copy and pasted from method-override.
*/
router.use(bodyParser.urlencoded({ extended: true }))
router.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}))

/*
Collecting given info, asking DB if there is already existing such combination of datasets.
If checks are passed, registering new user in database and redirecting him to login page.
Redirects to login page (instead on main page) with a reason to correctly save session info.
*/
router.route('/')
    .get(function (req, res, next) {
        const data = {
            "username": "",
            "email": "",
            "password": "",
            "confirmPassword": ""
        }
        res.render('auth/register', {
            "data": data,
            "title": "Register",
        });
    })
    .post(function (req, res) {
        const username = req.body.username;
        const email = req.body.email;
        const password = req.body.password;
        const confirmPassword = req.body.confirmPassword;

        const data = {
            "username": username,
            "email": email,
            "password": password,
            "confirmPassword": confirmPassword
        }

        if (password !== confirmPassword) {
            const error = "Passwords do not match!"
            res.format({
                html: function () {
                    res.render('auth/register', {
                        "error": error,
                        "data": data,
                        "title": "Register",
                    });
                }
            });
        } else {
            mongoose.model('User').findOne({
                $or: [
                    { username: username },
                    { email: email },
                ]
            }, function (err, user) {
                if (err) {
                    return console.error(err);
                } else {
                    if (user) {
                        const error = "User with same username/email already exists!"
                        res.format({
                            html: function () {
                                res.render('auth/register', {
                                    "error": error,
                                    "data": data,
                                    "title": "Register",
                                });
                            },
                            json: function () {
                                res.json(data);
                            }
                        });
                    }
                    else {
                        mongoose.model('User').create({
                            username: username,
                            email: email,
                            password: password,
                        }, function (err, project) {
                            if (err) {
                                res.send("There was a problem adding the information to the database.");
                            } else {
                                console.log('User registered: ' + username);
                                res.format({
                                    html: function () {
                                        res.location("login");
                                        res.redirect("/login");
                                    }
                                });
                            }
                        })
                    }
                }
            });
        }
    });

module.exports = router;
