/*
Any action, such as creating, modifying or deleting projects needs user validation first!
That action is validated with redirectIfNotLoggedIn();
*/

var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override');

/*
Any requests to this controller must pass through this 'use' function
Copy & pasted from method-override
*/
router.use(bodyParser.urlencoded({ extended: true }))
router.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
    }
}))

/*
/manage routing is used for interface that shows current user all projects he is participating in.
Shown projects are if he is author of a project or a member of same one.
Correspoding to one of those two options, he is given different page for modifying project.
*/
router.route('/manage')
    .get(function (req, res, next) {
        if (redirectIfNotLoggedIn(req, res)) return;

        const uid = req.session.uid.toString();

        mongoose.model('User').findById(uid, function (err, user) {
            if (err) {
                return console.error(err);
            } else {
                mongoose.model('Project').find({ author: uid }, function (err, leaderProjects) {
                    if (err) {
                        return console.error(err);
                    } else {
                        mongoose.model('Project').find({ "members": { '$regex': user.username, '$options': 'i' } }, function (err, memberProjects) {
                            if (err) {
                                return console.error(err);
                            } else {
                                res.format({
                                    html: function () {
                                        res.render('projects/manage', {
                                            title: 'My Projects',
                                            "leaderProjects": leaderProjects,
                                            "memberProjects": memberProjects,
                                        });
                                    },
                                    json: function () {
                                        res.json(leaderProjects);
                                        res.json(memberProjects);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    });

function redirectIfNotLoggedIn(req, res) {
    if (!req.session.uid) {
        res.redirect('/login');
        return true;
    }
    return false;
}

/*
Starting route for projects/index view which is HOME page of project.
ON START: Fetching all projects for DB and passing it to correspoding view for further processing.
ON SUBMIT: Collecting all the data and creating POST request with project info data. 
*/
router.route('/')
    .get(function (req, res, next) {
        if (redirectIfNotLoggedIn(req, res)) return;

        mongoose.model('Project').find({}, function (err, projects) {
            if (err) {
                return console.error(err);
            } else {
                mongoose.model('User').find({}, function (err, users) {
                    if (err) {
                        return console.error(err);
                    } else {
                        for (project of projects) {
                            for (user of users) {
                                if (project.author.toString() == user._id.toString()) {
                                    project.authorName = user.username;
                                    break;
                                }
                            }
                        }

                        res.format({
                            html: function () {
                                res.render('projects/index', {
                                    title: 'Projects',
                                    "projects": projects
                                });
                            },
                            json: function () {
                                res.json(projects);
                            }
                        });
                    }
                });
            }
        });
    })
    .post(function (req, res) {
        if (redirectIfNotLoggedIn(req, res)) return;

        const name = req.body.name;
        const description = req.body.description;
        const price = req.body.price;
        const _members = req.param('member');
        let members;
        if (typeof _members === 'undefined') {
            members = ""
        } else {
            members = _members.toString();
        }
        const finishedWorks = req.body.finishedWorks;
        const startTime = req.body.startTime;
        const endTime = req.body.endTime;
        const archived = req.body.archived === "on";
        const author = req.session.uid;

        mongoose.model('Project').create({
            name: name,
            description: description,
            price: price,
            members: members,
            finishedWorks: finishedWorks,
            startTime: startTime,
            endTime: endTime,
            archived: archived,
            author: author,
        }, function (err, project) {
            if (err) {
                res.send("There was a problem adding the information to the database.");
            } else {
                console.log('POST creating new project: ' + project);
                res.format({
                    html: function () {
                        // If it worked, set the header so the address bar doesn't still say /adduser
                        res.location("projects");
                        // And forward to success page
                        res.redirect("/projects");
                    },
                    json: function () {
                        res.json(project);
                    }
                });
            }
        })
    });

/*
/archive route is used for showing current users archived projects.
Shown projects are only that user participated in.
*/
router.route('/archive')
    .get(function (req, res, next) {
        if (redirectIfNotLoggedIn(req, res)) return;

        const uid = req.session.uid.toString();

        mongoose.model('User').findById(uid, function (err, user) {
            if (err) {
                return console.error(err);
            } else {
                mongoose.model('Project').find({
                    archived: true,
                    $or: [
                        { author: uid },
                        { "members": { '$regex': user.username, '$options': 'i' } }
                    ]
                }, function (err, projects) {
                    if (err) {
                        return console.error(err);
                    } else {
                        res.format({
                            html: function () {
                                res.render('projects/archive', {
                                    title: 'My Archived Projects',
                                    "projects": projects
                                });
                            },
                            json: function () {
                                res.json(projects);
                            }
                        });
                    }
                });
            }
        });
    })

/*
/new route is simple redirection route that calls main page while passing given info,
that will be saved in database.
*/
router.get('/new', function (req, res) {
    if (redirectIfNotLoggedIn(req, res)) return;

    const uid = req.session.uid.toString();

    mongoose.model('User').find({ _id: { $ne: uid } }, function (err, users) {
        if (err) {
            console.log('GET Error: There was a problem retrieving: ' + err);
        } else {
            res.render('projects/new', {
                title: 'New Project',
                users: users,
            });
        }
    });
});

/*
Simple Jade interface for showing specific project info to user.
*/
router.route('/:id')
    .get(function (req, res) {
        if (redirectIfNotLoggedIn(req, res)) return;

        const uid = req.session.uid.toString();

        mongoose.model('User').findById(uid, function (err, user) {
            if (err) {
                return console.error(err);
            } else {
                mongoose.model('Project').findById(req.params.id, function (err, project) {
                    if (err) {
                        console.log('GET Error: There was a problem retrieving: ' + err);
                    } else {
                        res.format({
                            html: function () {
                                res.render('projects/show', {
                                    "project": project,
                                    "author": user.username,
                                    "title": "Details",
                                });
                            },
                            json: function () {
                                res.json(project);
                            }
                        });
                    }
                });
            }
        });
    });

/*
Divided interface for editting or deleting specific project info.
Call with value='DELETE' removes specific project from database, while other leads to user interface where same project can be updated with new info.
This functionalities are now divided in two routes because of reusability in other views.
*/
router.route('/edit/:id')
    .get(function (req, res) {
        if (redirectIfNotLoggedIn(req, res)) return;

        const uid = req.session.uid.toString();

        mongoose.model('User').find({ _id: { $ne: uid } }, function (err, users) {
            if (err) {
                console.log('GET Error: There was a problem retrieving: ' + err);
            } else {
                mongoose.model('Project').findById(req.params.id, function (err, project) {
                    if (err) {
                        console.log('GET Error: There was a problem retrieving: ' + err);
                    } else {
                        for (user of users) {
                            const members = project.members;
                            if (members === "" || members === null) {
                                user.checked = false;
                            } else {
                                user.checked = members.includes(user.username);
                            }
                        }

                        res.format({
                            html: function () {
                                res.render('projects/edit', {
                                    title: 'Project: ' + project._id,
                                    "project": project,
                                    "users": users,
                                    "title": "Edit",
                                });
                            },
                            json: function () {
                                res.json(project);
                                res.json(users);
                            }
                        });
                    }
                });
            }
        });
    })
    .put(function (req, res) {
        if (redirectIfNotLoggedIn(req, res)) return;

        const name = req.body.name;
        const description = req.body.description;
        const price = req.body.price;
        const _members = req.param('member');
        let members;
        if (typeof _members === 'undefined') {
            members = ""
        } else {
            members = _members.toString();
        }
        const finishedWorks = req.body.finishedWorks;
        const startTime = req.body.startTime;
        const endTime = req.body.endTime;
        const archived = req.body.archived === "on";

        mongoose.model('Project').findById(req.params.id, function (err, project) {
            project.update({
                name: name,
                description: description,
                price: price,
                members: members,
                finishedWorks: finishedWorks,
                startTime: startTime,
                endTime: endTime,
                archived: archived,
            }, function (err, projectId) {
                if (err) {
                    res.send("There was a problem updating the information to the database: " + err);
                } else {
                    res.format({
                        html: function () {
                            res.redirect("/projects/manage");
                        }
                    });
                }
            })
        });
    });

/*
Route specialized for giving only specific parameters to be changed from currently logged user.
*/
router.route('/edit_restricted/:id')
    .get(function (req, res) {
        if (redirectIfNotLoggedIn(req, res)) return;

        const uid = req.session.uid.toString();

        mongoose.model('User').find({ _id: { $ne: uid } }, function (err, users) {
            if (err) {
                console.log('GET Error: There was a problem retrieving: ' + err);
            } else {
                mongoose.model('Project').findById(req.params.id, function (err, project) {
                    if (err) {
                        console.log('GET Error: There was a problem retrieving: ' + err);
                    } else {
                        for (user of users) {
                            const members = project.members;
                            if (members === "" || members === null) {
                                user.checked = false;
                            } else {
                                user.checked = members.includes(user.username);
                            }
                        }

                        res.format({
                            html: function () {
                                res.render('projects/edit_restricted', {
                                    title: 'Project: ' + project._id,
                                    "project": project,
                                    "users": users,
                                    "title": "Edit",
                                });
                            },
                            json: function () {
                                res.json(project);
                                res.json(users);
                            }
                        });
                    }
                });
            }
        });
    })
    .put(function (req, res) {
        if (redirectIfNotLoggedIn(req, res)) return;

        const finishedWorks = req.body.finishedWorks;

        mongoose.model('Project').findById(req.params.id, function (err, project) {
            project.update({
                finishedWorks: finishedWorks,
            }, function (err, projectId) {
                if (err) {
                    res.send("There was a problem updating the information to the database: " + err);
                } else {
                    res.format({
                        html: function () {
                            res.redirect("/projects/manage");
                        }
                    });
                }
            })
        });
    });

/*
Interface user for searching for project with given ID and removing it from database permanently.
*/
router.route('/delete/:id')
    .delete(function (req, res) {
        if (redirectIfNotLoggedIn(req, res)) return;

        mongoose.model('Project').findById(req.params.id, function (err, project) {
            if (err) {
                return console.error(err);
            } else {
                project.remove(function (err, project) {
                    if (err) {
                        return console.error(err);
                    } else {
                        console.log('DELETE removing ID: ' + project._id);
                        res.format({
                            html: function () {
                                res.redirect("/projects/manage");
                            },
                            json: function () {
                                res.json({
                                    message: 'deleted',
                                    item: project
                                });
                            }
                        });
                    }
                });
            }
        });
    });



module.exports = router;